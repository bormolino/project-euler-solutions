//Output: Result: 872187
//Runtime: real	0m0.301s

public final class Prob36 {

    private final static boolean isPalindrome(String s) {
    	return new StringBuffer(s).reverse().toString().equals(s);
    }

    public static void main(String[] args) {
    	int sum = 0;

    	for(int i = 1; i < 1000000; i++) 
    	    if(isPalindrome(String.valueOf(i)))
        		if(isPalindrome(Integer.toBinaryString(i)))
        		    sum += i;

    	System.out.println("Result: "+sum);
    }
}
