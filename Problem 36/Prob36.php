<?php

//Output: 872187
//Runtime: real	0m1.238s

$result = 0;

for($i = 0; $i <= 1000000; $i++)
   if(decbin($i) == strrev(decbin($i)) && $i == strrev($i))
      $result += $i;

print $result;

?>
