#include <stdio.h>

/*
 * gcc -Wall -O3 -march=native -mtune=native -std=c11 Prob493.c -lm -o Prob493
 * Output: Result: 6.818741802
 * Runtime: real	0m0.001s
 */

double factorial(double n) {
    return (n == 1) ? 1 : factorial(n - 1) * n;
}

double binomial(double n, double k) {
    return factorial(n) / (factorial(k) * factorial(n - k));
}

int main(void) {
    double result = 7 * (1 - binomial(60, 20) / binomial(70, 20));
    printf("Result: %.9f", result);

    return 0;
}
