#include <stdio.h>

/*
 * gcc -O3 -march=native -mtune=native -std=c11 -ffast-math prob3.c -lm -o prob3
 * Output: Result: 6857
 * Runtime: real	0m0.022s
 */

#define NUM 600851475143

typedef enum { false, true } bool;

inline static unsigned long long square(unsigned long long n) {
    return n * n;
}

static bool isPrime(unsigned long long n) {
    if(n == 1)
        return false;

    else if(n == 2)
        return true;

    else {
        unsigned long long i, r = (unsigned long long) __builtin_sqrt(n);

        for(i = 3; i <= r && n % i != 0; i+=2);

        return (r == i - 1) ? true : false;
    }
}

int main(void) {
    unsigned long long max_fac = 0;

    for(unsigned long long x = 1; square(x) <= NUM; ++x)
        if((NUM % x) == 0)
            if(isPrime(x) == true)
                if(x > max_fac)
                    max_fac = x;

    printf("Result: %llu\n", max_fac);
    return 0;
}
