//Output: Result: 428570/999997
//Runtime: real	1144m19.275s

public final class Prob71 {

    private final static int gcd(int a, int b) {
        return (b == 0) ? a : gcd(b, a % b);
  }
    
  public static void main(String[] args) {
    double dec = 3.0d / 7.0d, dec2 = 2.0d / 5.0d;
    String frac = "";
    
    for(int d = 2; d < 1_000_000; d++) {
      for(int n = 1; n < d; n++) {
        if(gcd(n,d) == 1) {
          double tmp = (double) n / (double) d;
          
          if(tmp < dec && tmp > dec2) {
            dec2 = tmp;
            frac = n+"/"+d;
          }
        }
      }
    }
    
    System.out.println("Result: "+frac);
  }
}
