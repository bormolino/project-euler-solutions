//Output: Result: -59231
//Runtime: real	0m0.464s

public class Prob27 {

    private static boolean is_prime(int n) {
        if(n == 1)
            return false;

        if(n == 2)
            return true;

        int r = (int) Math.floor(Math.sqrt(n));
        int i;

        for(i = 2; i <= r; ++i)
            if(n % i == 0)
                break;

        return (r == i - 1);
    }

    public static void main(String[] args) {
        int[] sequence = new int[2];
        int result = 0;

        for(int a = -1000; a <= 1000; a++) {
            for(int b = -1000; b <= 1000; b++) {
                int n = 0;

                while(is_prime(Math.abs(n * n + a * n + b)))
                    n++;

                if(n > result) {
                    result = n;
                    sequence[0] = a;
                    sequence[1] = b;
                }
            }
        }

        System.out.println("Result: "+sequence[0] * sequence[1]);
    }
}
