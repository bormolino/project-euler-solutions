<?php

//Output: Result: -59231
//Runtime: real	0m2.654s

function is_prime($n) {
	if($n == 1)
		return false;
		
	if($n == 2)
		return true;
		
	$r = floor(sqrt($n));
	$i;
	
	for($i = 2; $i <= $r; ++$i)
		if($n % $i == 0)
			break;
	
	return ($r == $i-1);
}

$sequence = array();
$result = 0;

for($a = -1000; $a <= 1000; $a++) {
	for($b = -1000; $b <= 1000; $b++) {
		$n = 0;
		
		while(is_prime(abs($n * $n + $a * $n + $b)))
			$n++;
			
		if($n > $result) {
			$result = $n;
			$sequence[0] = $a;
			$sequence[1] = $b;
		}
	}
}

print "Result: ".$sequence[0] * $sequence[1];
