<?php

//Output: Ergebnis: 233168
//Runtime: real	0m0.051s

$sum = 0;

for($i = 0; $i < 1000; $i++)
    if($i % 3 == 0 || $i % 5 == 0)
        $sum += $i;

echo "Ergebnis: ".$sum;

?>
