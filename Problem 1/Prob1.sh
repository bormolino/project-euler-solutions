#!/bin/bash

#Output: 233168
#Runtime: real	0m0.019s

SUM=0

for i in {1..999}
do
    if [ $((i % 3)) == 0 ] || [ $((i % 5)) == 0 ] ; then
	    SUM=$((SUM + i))
    fi
done

echo $SUM

