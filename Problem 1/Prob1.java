/*
 * Output: Result: 233168
 * Runtime: real	0m0.132s
 */

public class Prob1 {
    public static void main(String[] args) {
        int sum = 0;
        
        for(int i = 0; i < 1000; i++)
            if((i % 3 == 0) || (i % 5 == 0))
                sum += i;

        System.out.println("Result: "+sum);
    }
}
