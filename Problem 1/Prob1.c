#include <stdio.h>

/*
 * gcc -O3 -march=native -mtune=native -std=c11 -ffast-math Prob1.c -lm -o Prob1
 * Output: Result: 233168
 * Runtime: real	0m0.001s
 */

int main(void) {
    int sum;

    for(int i = sum = 0; i < 1000; i++)
        if((i % 3 == 0) || (i % 5 == 0))
            sum += i;

    printf("Result: %d\n", sum);

    return 0;
}
