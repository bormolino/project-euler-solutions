#!/usr/bin/env perl

#Output: Result: 24702 !
#Runtime: real	0m13.558s

use strict;
use warnings;
use Games::Sudoku::Solver qw(:Minimal set_solution_max);

my $fh;
my $sudokus = {};
my $current_idx;
my $res;

open $fh, "sudoku.txt" or die($!);

while (<$fh>) {
    chomp($_);
    if($_ =~ m/^Grid (\d+)/) {
        $current_idx = $1;
        $sudokus->{$current_idx} = [];
        next;
    }
    push $sudokus->{$current_idx}, split '', $_;
}

print "Calculate the results ...\n";

foreach (keys $sudokus) {
    my @temp;
    my @result;

    sudoku_set \@temp, \@{$sudokus->{$_}};
    set_solution_max 1;
    sudoku_solve \@temp, \@result;
    $res += 100 * $result[0][0][0] + 10 * $result[0][0][1] + $result[0][0][2];
}

print "Result: $res !\n";
