import java.math.BigInteger;

//Output: Found a number: a21 = 27512614111
//Runtime: - 

public class Prob119v2 {

    public final static BigInteger getDigitSum(BigInteger n) {
        BigInteger sum = BigInteger.ZERO;

        while(n.compareTo(BigInteger.ZERO) == 1) {
            sum = sum.add(n.mod(new BigInteger("10")));
            n = n.divide(new BigInteger("10"));
        }

        return sum;
    }

    public static void main(String[] args) {
        o: for (BigInteger v = new BigInteger("27512614111"), c = new BigInteger("21");
            c.compareTo(new BigInteger("30")) == -1; v = v.add(BigInteger.ONE)) {

            BigInteger qs = getDigitSum(v);
            BigInteger z = v.divide(qs);
            //System.out.printf("v = %d, qs = %d, z = %d%n", v, qs, z);

            if ((qs.compareTo(BigInteger.ONE) == 0) || (v.mod(qs)).compareTo(BigInteger.ZERO) != 0) {
                continue;
            }

            for (BigInteger x = qs; x.compareTo(v) <= 0;) {
                if (x.compareTo(v) == 0) {
                    System.out.printf("Found a number: a%s = %s%n", c.toString(), v.toString());
                    c = c.add(BigInteger.ONE);
                    continue o;
                }
            
                x = x.multiply(qs);
            }
        }
    }

}
