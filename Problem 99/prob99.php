<?php

//Output: 709
//Runtime: real	0m0.061s

$result = 0;
$pair = preg_split("/[\n\r]+/", file_get_contents("base_exp.txt"));

for($i = 0; $i < count($pair); $i++){
    preg_match("/(\d+),(\d+)/", $pair[$i], $match);
    $partresult = log($match[1]) * $match[2];

    if($partresult > $result) {
        $result = $partresult;
	$line = $i+1;
    }
}

print $line;

?>
