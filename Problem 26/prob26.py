#Output: Result:  983
#Runtime: real	0m0.553s

result = [0, 0]

for i in range(1, 1001):
    a = [0, 1000]
    num = 10 % i

    while num != 1 and a[1] > 0:
        num *= 10
        num %= i
        a[0] += 1
        a[1] -= 1

    if a[0] > result[0] and a[1] > 1:
        result[0] = a[0]
        result[1] = i

print("Result: ", result[1])
