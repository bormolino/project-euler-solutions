#!/usr/bin/perl -w

#Output: 669171001
#Runtime: real	0m0.003s

sub solve {
    $n = int(($_[0] - 1) / 2);
    return int((16 * $n**3 + 30 * $n**2 + 26 * $n + 3) / 3);
}

$res = solve(1001);

print $res;
