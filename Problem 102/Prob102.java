//Output: Result: 228
//Runtime: real	0m0.445s

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public final class Prob102 {

    private final static int areaOfTriangle(int ax, int ay, int bx, int by, int cx, int cy) {
        return Math.abs((ax - cx) * (by - ay) - (ax - bx) * (cy - ay));
    }

    private final static boolean triangleContainsPoint(int ax, int ay, int bx, int by, int cx, int cy, int px, int py) {
        return (areaOfTriangle(ax, ay, bx, by, cx, cy) ==
                areaOfTriangle(ax, ay, bx, by, px, py) +
                areaOfTriangle(ax, ay, px, py, cx, cy) +
                areaOfTriangle(px, py, bx, by, cx, cy));
    }

    public static void main(String[] args) {
        String[] l = null;
        int result = 0;

        try {
            final byte[] f = Files.readAllBytes(Paths.get("triangles.txt"));
            l = (new String(f, StandardCharsets.UTF_8)).split("\\r?\\n");
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < l.length; i++) {
            String[] c = l[i].split(",");
            int[] C = Arrays.stream(c).mapToInt(Integer::parseInt).toArray();

            if (triangleContainsPoint(C[0], C[1], C[2], C[3], C[4], C[5], 0, 0))
                result++;
        }

        System.out.println("Result: " + result);
    }
}
