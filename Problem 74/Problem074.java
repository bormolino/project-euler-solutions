import java.util.ArrayList;

/*
 * The number 145 is well known for the property that the sum of the factorial of its digits is equal
 * to 145: 1! + 4! + 5! = 1 + 24 + 120 = 145
 * Perhaps less well known is 169, in that it produces the longest chain of numbers that link back to 169;
 * it turns out that there are only three such loops that exist:
 * 169 → 363601 → 1454 → 169
 * 871 → 45361 → 871
 * 872 → 45362 → 872
 * It is not difficult to prove that EVERY starting number will eventually get stuck in a loop. For example,
 * 69 → 363600 → 1454 → 169 → 363601 (→ 1454)
 * 78 → 45360 → 871 → 45361 (→ 871)
 * 540 → 145 (→ 145)
 * Starting with 69 produces a chain of five non-repeating terms, but the longest non-repeating chain
 * with a starting number below one million is sixty terms.
 * 
 * How many chains, with a starting number below one million, contain exactly sixty non-repeating terms?
 *
 * Output: Result: 402
 * Runtime: real	0m8.086s
 */
public class Problem074 {
	private static int[] fac = new int[] {1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880};
	
	public static void main(String[] args) {
		int c = 0;
		
		for (int i = 1; i < 1_000_000; i++) {
			if (getChainLength(i) == 60) {
				c++;
			}
		}
		
		System.out.println("Result: " + c);
	}
	
	private static int[] getDigits(int n) {
		int[] digits = new int[(int)(Math.log10(n)+1)];
		int i = 0;
		
		while (n > 0) {
			digits[i++] = n % 10;
			n /= 10;
		}
		
		return digits;
	}
	
	private static int getDigitFacSum(int n) {
		int[] digits = getDigits(n);
		int sum = 0;
		
		for (int i : digits) {
			sum += fac[i];
		}
		
		return sum;
	}
	
	private static int getChainLength(int n) {
		int tmp = getDigitFacSum(n), i = 1;
		ArrayList<Integer> list = new ArrayList<>();
		
		while (tmp != n && i < 60 && !list.contains(tmp)) {
			list.add(tmp);
			tmp = getDigitFacSum(tmp);
			i++;
		}
		
		return i;
	}
}
