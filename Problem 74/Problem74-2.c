#include <stdio.h>
#include <math.h>

/* 
 * gcc -O3 -march=native -mtune=native -std=c11 -ffast-math Problem74-2.c -lm -o Problem74-2
 * Output: Result: 402
 * Runtime: real	0m1.553s
 */

typedef unsigned long ulong;

static const ulong faclist[] = {1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880};

static inline ulong getDigitFacSum(ulong n) {
    ulong sum;

    for(sum = 0; n > 0; sum += faclist[n % 10], n /= 10);

    return sum;
}

static ulong getChainLength(ulong n) {
    ulong checklist[60], count = 1, i;

    if((*checklist = getDigitFacSum(n)) == n)
        return count;

    do {
        checklist[count] = getDigitFacSum(checklist[count-1]);

        for(i = 0; i < count; i++)
            if(checklist[count] == checklist[i])
                return ++count;
    } while(++count < 60);

    return 0;
}

int main(void) {
    ulong c = 0, i;

    for(i = 1; i < 1000000; i++)
        if(getChainLength(i) == 60)
            c++;

    printf("Result: %lu\n", c);
}
