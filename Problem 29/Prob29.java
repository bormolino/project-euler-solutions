import java.util.ArrayList;

//Output: Result: 9183
//Runtime: real	0m0.267s

public class Prob29 {
    public static void main(String[] args) {
        ArrayList<Double> list = new ArrayList<Double>();
        
        for(int a = 2; a <= 100; a++) {
            for(int b = 2; b <= 100; b++) {
                double pow = (double) Math.pow(a, b);
                
                if(!list.contains(pow))
        		    list.add(pow);
            }
        }

        int result = list.size();
        System.out.println("Result: "+result);
    }
}
