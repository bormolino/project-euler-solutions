#!/usr/bin/env perl

#Output: Result: 9183
#Runtime: real	0m3.297s

use strict;
use warnings;

my @nums = ();

foreach my $a (2..100) {
    foreach my $b (2..100) {
        my $num = $a ** $b;
        if(!(grep $num == $_, @nums)) {
            push @nums, $num;
        }
    }
}

print "Result: ", scalar @nums, "\n";


