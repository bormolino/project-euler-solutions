#include <stdio.h>

/*
 * gcc -O3 -march=native -mtune=native -std=c11 -ffast-math Prob6.c -lm -o Prob6
 * Output: Result: 25164150.000000
 * Runtime: real	0m0.002s
 */

int main(void) {
    double part1 = 0, part2 = 0, result = 0;
    int i;

    for(i = 1; i <= 100; i++) {
        part1 += i * i;
        part2 += i;
    }

    result = (part2 * part2) - part1;
    
    printf("Result: %f", result);
    return 0;
}
