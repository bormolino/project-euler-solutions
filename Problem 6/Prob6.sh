#!/usr/bin/env bash

#Output: 25164150
#Runtime: real	0m0.003s

PART1=0
PART2=0
RESULT=0

for i in {1..100}
do
    PART1=$((PART1 + i * i))
    PART2=$((PART2 + i))
done

RESULT=$((PART2 * PART2 - PART1))

echo $RESULT
