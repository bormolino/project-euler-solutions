#include <stdio.h>

/*
 * gcc -O3 -march=native -mtune=native -std=c11 -ffast-math -fopenmp Prob50.c -lm -o Prob50
 * Output: Result: 997651 with 543 terms
 * Runtime: real	0m5.862s
 */

#define ARRLENGTH(x) (sizeof(x) / sizeof((x)[0]))

static unsigned int prmlst[78498];

inline static unsigned int isPrime(unsigned int n) {
    if(n == 1)
        return 0;

    else if(n == 2)
        return 1;

    else {
        int i, r = (int) __builtin_sqrt(n);

        for(i = 2; i <= r && n % i != 0; ++i);

        return (r == i - 1) ? 1 : 0;
    }
}

inline static unsigned int getConPrmCnt(unsigned int n) {
    unsigned int sum = 2, prm = 1, cnt = 1, sumcnt = 0;

    while(sum != n) {
        sum = prmlst[sumcnt++];
        prm = 1;
        cnt = sumcnt;

        while(sum < n) {
            sum += prmlst[cnt++];
            prm++;
        }
    }

    return (sum == n) ? prm : 0;
}

int main(void) {
    unsigned int idx = 0, res = 0;

    for(unsigned int i = 0; i < 1000000; i++)
        if(isPrime(i))
            prmlst[idx++] = i;

    #pragma omp parallel for
    for(unsigned int i = 0; i < ARRLENGTH(prmlst); i++) {
        unsigned int tmp = getConPrmCnt(prmlst[i]);

        if(tmp > res) {
            res = tmp;
            idx = prmlst[i];
        }
    }

    printf("Result: %u with %u terms\n", idx, res);
}
