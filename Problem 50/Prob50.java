//Output: Initialisiere alle Primzahlen unter 1 Mio ...
//Initialisierung abgeschlossen!
//Länge: 78498
//Starte Primetest ...
//Abgeschlossen!
//Result: Glieder = 543 , Primzahl = 997651

//Runtime: real	0m28.095s

public final class Prob50 {

    private static int[] primeList = new int[78498];

    private static boolean isPrime(int n) {
        if(n == 1)
            return false;
 
        if(n == 2)
            return true;
 
        int r = (int) Math.floor(Math.sqrt(n));
        int i;

        for(i = 2; i <= r && n % i != 0; ++i) {}
 
        return (r == i - 1);
    }

    private static int getConsecutivePrimesCount(int n) {
        int sum = 2, cnt = 1, sumcnt = 0, anotherCounter = 1;

        while(sum != n) {
            sum = primeList[sumcnt++];
            cnt = 1;
            anotherCounter = sumcnt;

            while(sum < n) {
                sum += primeList[anotherCounter++];
                cnt++;
            }    
        }
        
        return (sum == n) ? cnt : -1;
    }
            

    public static void main(String[] argv) {
        int cnt = 0, res = 0; 

        System.out.println("Initialisiere alle Primzahlen unter 1 Mio ...");

        for(int i = 0; i < 1_000_000; i++)
            if(isPrime(i))
                primeList[cnt++] = i;

        System.out.println("Initialisierung abgeschlossen!");
        System.out.println("Länge: "+primeList.length);
        System.out.println("Starte Primetest ...");

        for(int i = 0; i < primeList.length; i++) {
            int tmp = getConsecutivePrimesCount(primeList[i]);

            if(tmp > res) {
                res = tmp;
                cnt = primeList[i];
            }
        }

        System.out.println("Abgeschlossen!\nResult: Glieder = "+res+" , Primzahl = "+cnt);
    }
}
