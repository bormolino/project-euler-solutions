isqrt :: Integer -> Integer
isqrt = floor . sqrt . fromIntegral

isPrim :: Integer -> Bool
isPrim p = if p > 1 then null [ x | x <- [2..isqrt p], p `mod` x == 0] else False

solve = sum [x | x <- [2..1999999], isPrim x]
