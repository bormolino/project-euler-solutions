#include <stdio.h>

/*
 * gcc -O3 -march=native -mtune=native -std=c11 -ffast-math prob10.c -lm -o prob10
 * Output: 142913828922.000000
 * Runtime: real	23m55.350s
 */

int main(void) {

    long k, p;
    int count = -1;
    double all = 0;

     for (long i = 1; i <= 100000000; i++) {
        for (p = 1, k = 2; k < i; k++) {
            if (i % k == 0) {
                p = 0;
                break;
              }
        }

        if (p == 1) {
            printf("Found prime: %ld (prime number %i)\n", i, count);
            count++;
	        all += i;
	    }
	   
        if(i > 2000000) {
		    printf("\n\nResult found: %f\n\n", all-1);
		    break;
	    }
    }

    return 0;
}

