//Output: Result found: 5482660
//Runtime: real	0m0.128s

public class Prob44 {

    //thanks to http://en.wikipedia.org/wiki/Pentagonal_number
    private static boolean isPentagonal(int n) {
        double x = (Math.sqrt(24 * n + 1) + 1) / 6;
        return (x == (int)x);
    }

    public static void main(String[] args) {
        int i = 1;
        boolean run = true;

        while(run) {
            ++i;
            int n = i * (3 * i - 1) / 2;

            for(int j = i - 1; j > 0; j--) {
                int d = j * (3 * j - 1) / 2;

                if(isPentagonal(n - d) && isPentagonal(n + d)) {
                    System.out.println("Result found: "+(n - d));
                    run = false;
                    break;
                }
            }
        }
    }
}
