#include <stdio.h>

/* 
 * gcc -O3 -march=native -mtune=native -std=c11 -ffast-math Prob44.c -lm -o Prob44
 * Output: Result found: 5482660
 * Runtime: real	0m0.022s
 */


typedef enum { false, true } bool;

bool isPentagonal(long n) {
    double x = (__builtin_sqrt(24 * n + 1) + 1) / 6;
    return (x == (long) x) ? true : false;
}

int main(void) {
    bool flag = true;

    for(int i = 1; flag == true; ++i) {
        int n = i * (3 * i - 1) / 2;

        for(int j = i - 1; j > 0; j--) {
            int d = j * (3 * j - 1) / 2;

            if(isPentagonal(n - d) == true && isPentagonal(n + d) == true) {
                printf("Result found: %d\n", (n - d));
                flag = false;
            }
        }
    }
}
