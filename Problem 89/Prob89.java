//Output: Result: 743
//Runtime: real	0m0.379s

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Prob89 {
    private static String romanNumbers;
    private static String[] singleRomanNumbers;

    public static void main(String[] args) {
        try {
            final byte[] encoded = Files.readAllBytes(Paths.get("roman.txt"));
            romanNumbers = new String(encoded, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }

        singleRomanNumbers = romanNumbers.split("\\r?\\n");

        for (int i = 0; i < singleRomanNumbers.length; i++) {
            singleRomanNumbers[i] = singleRomanNumbers[i].replace("DCCCC", "CM");
            singleRomanNumbers[i] = singleRomanNumbers[i].replace("LXXXX", "XC");
            singleRomanNumbers[i] = singleRomanNumbers[i].replace("VIIII", "IX");
            singleRomanNumbers[i] = singleRomanNumbers[i].replace("CCCC", "CD");
            singleRomanNumbers[i] = singleRomanNumbers[i].replace("XXXX", "XL");
            singleRomanNumbers[i] = singleRomanNumbers[i].replace("IIII", "IV");
        }

        StringBuilder b = new StringBuilder();

        for (String s : singleRomanNumbers)
            b.append(s);

        String newRomanNumbers = b.toString();

        final int result = romanNumbers.replace("\n", "").length() - newRomanNumbers.length();
        System.out.println("Result: " + result);
    }
}
