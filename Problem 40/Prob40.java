//Output: Result: 210
//Runtime: real	0m0.130s

public final class Prob40 {

    public static void main(String[] argv) {

        StringBuilder sb = new StringBuilder();

        for(int i = 1; i < 190_000; i++)
            sb.append(i);

        String d = sb.toString();
        int res = (d.charAt(0)-'0') * (d.charAt(9)-'0') * (d.charAt(99)-'0') * (d.charAt(999)-'0') * (d.charAt(9999)-'0') * (d.charAt(99999)-'0') *  (d.charAt(999999)-'0');

        System.out.println("Result: "+res);
    }
}
