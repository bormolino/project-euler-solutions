#include <stdio.h>

/*
 * gcc -O3 -march=native -mtune=native -std=c11 -ffast-math -fopenmp prob53.c -lm -o prob53
 * Output: Result: = 4075.000000 
 * Runtime: real	0m0.003s
 */

double factorial(double n) {
    double f;
    for (f = n; n > 1; f *= --n);
    return f;
}

double binomial(double n, double k) {
    return factorial(n) / (factorial(k) * factorial(n - k));
}

int main(void) {
    double n, r, sum;
    
    for(n = 1; n <= 100; n++)
    	for(r = 1; r < n; r++)
    	    if(binomial(n, r) > 1000000)
        		sum++;

    printf("Result: = %f \n", sum);

}
