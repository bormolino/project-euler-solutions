#include <stdio.h>
#include <stdint.h>

/*
 * gcc -O3 -march=native -mtune=native -std=c11 -ffast-math -fopenmp p53.c -lm -o p53
 * Output: Result: 4075
 * Runtime: real	0m0.004s
 */

inline static __int128 binomial(int n, int k) {
    __int128 result;
    int i;

    if(k == 0)
        return 1;

    if(k + k > n)
        return binomial(n, n - k);

    for(result = n - k + 1, i = 2; !(i > k); i++) {
        result *= (n - k + i);
        result /= i;
    }

    return result;
}

int main(void) {
    int sum = 0;

    for(int n = 1; !(n > 100); n++)
        for(int r = 1; !(r > n); r++)
            if(binomial(n, r) > 1000000)
                sum++; 

    printf("Result: %d\n", sum);

    return 0;
}
