<?php

//Output: Result 31626
//Runtime: real	0m2.462s

function divSum($n) {
    $s = 0; $i = 1;

    while($i < $n) {
        if($n % $i == 0)
            $s = $s + $i;

        $i++;
    }

    return $s;
}

function amicable_nums($low, $high) {
    $a = $low; $b; $sum = 0;

    while($a <= $high) {
        $b = divSum($a);

        if($b > $a && divSum($b) == $a)
            $sum = $sum + $a + $b;

        $a++;
    }

    return $sum;
}

$result = amicable_nums(1, 10000);
echo "Result ".$result."\n";

?>
