//Output: 31626
//Runtime: real	0m0.685s

public class Prob21 {

    private static int divSum(int n) {
        int s = 0, i = 1;

        while(i < n) {
            if(n % i == 0)
                s = s + i;

            i++;
        }

        return s;
    }
        
    private static int amicable_nums(int low, int high) {
        int a = low, b, sum = 0;

        while(a <= high) {
            b = divSum(a);

            if(b > a && divSum(b) == a)
                sum = sum + a + b;

            a++;
        }

        return sum;
    }

    public static void main(String[] args) {
        int result = amicable_nums(1, 10000);
        System.out.println(result);
    }
}
