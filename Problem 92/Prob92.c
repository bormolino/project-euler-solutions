#include <stdio.h>

/*
 * gcc -O3 -march=native -mtune=native -std=c11 -ffast-math Prob92.c -lm -o Prob92
 * Output: Result: 8581146
 * Runtime: real	0m1.051s
 */

typedef unsigned int uint;

__inline static uint square(uint x) {
    return x * x;
}

__inline static uint solve(uint num) {
    register uint res = 0, val = num;

    while(val > 0) {
        res += square(val % 10);
        val /= 10;
    }

    return res;
}

int main() {
    typedef enum { false, true } bool;
    uint result = 0, i = 0, tmp = 0;

    for(i = 10000000; i != 0; i--) {
        tmp = solve(i);
        bool run = true;

        while(run) {
            switch(tmp) {
                case 1: run = false; break;
                case 89: result++; run = false; break;
                default: tmp = solve(tmp);
            }
        }
    }
    printf("Result: %i", result);
    return 0;
}
