//Output: Result: 55
//Runtime: real	0m1.489s

public final class Prob35 {

    private final static boolean isPrime(int n) {
        if(n == 1)
            return false;
 
        if(n == 2)
            return true;
 
        int r = (int) Math.floor(Math.sqrt(n));
        int i;

        for(i = 2; i <= r && n % i != 0; ++i) {}
 
        return (r == i - 1);
    }

    private final static boolean isCircular(int n) {
        String s = Integer.toString(n);

        for (int i = 0; i < s.length(); i++)
            if(!isPrime((Integer.parseInt(s.substring(i) + s.substring(0, i)))))
                return false;

        return true;
    }

    public static void main(String[] argv) {
        int cnt = 0; 

        for(int i = 0; i < 1_000_000; i++)
            if(isPrime(i))
                if(isCircular(i))
                    ++cnt;

        System.out.println("Result: "+cnt);
    }
}

