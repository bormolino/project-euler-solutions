#include <stdio.h>

/*
 * gcc -O3 -march=native -mtune=native -std=c11 -ffast-math prob73.c -lm -o prob73
 * Output: result: 7286375
 * Runtime: real	0m0.852s
 */

static long gcd(long a, long b) {
    long r = a % b;

    return (r == 0)? b: gcd(b, r);
}

int main(void) {
    long d, n, s, sum;

    for (sum = 0, d = 3; d <= 12000; d++) {
        for (s = 0, n = d / 3 + 1; n < (d-1) / 2; n++) {
            s += gcd(d, n) == 1;
        }
        sum += s;
    }

    printf("\nresult: %ld\n", sum);

    return 0;
}
