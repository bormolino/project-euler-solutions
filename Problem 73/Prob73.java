import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

// Output: Filling hashmap ... done
//Sorting hashmap ... done
//Calculating result ... done
//Result is: 7295372
// Runtime: real	0m55.535s


public final class Prob73 {

    private final static int gcd(int a, int b) {
        return (b == 0) ? a : gcd(b, a % b);
    }

    private final static HashMap sortByValues(HashMap map) { 
        List list = new LinkedList(map.entrySet());

        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(((Map.Entry) (o2)).getValue());
            }
        });

        HashMap sortedHashMap = new LinkedHashMap();

        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
 
        return sortedHashMap;
    }

    public static void main(String[] args) {
        HashMap<String, Double> resmap = new HashMap<String, Double>();
        String tmp;
        double temp;

        System.out.print("Filling hashmap ...");
        for(int d = 2; d <= 12000; d++) {
            for(int n = 1; n < d; n++) {
                if(gcd(n,d) == 1) {
                    tmp = n+"/"+d;
                    temp = (double) n / (double) d;

                    if(temp > 0.30d && temp < 0.50d)
                        resmap.put(tmp, temp);
                }
            }
        }
        System.out.println(" done");

        System.out.print("Sorting hashmap ...");
        resmap = sortByValues(resmap);
        System.out.println(" done");

        System.out.print("Calculating result ...");
        Set set = resmap.entrySet();
        Iterator it = set.iterator();
        boolean flag = false;
        int result = 0;
        
        while(it.hasNext()) {
            Map.Entry mentry = (Map.Entry) it.next();

            if(flag)
                result++;
                
            if(mentry.getKey().equals("1/3"))
                flag = true;

            if(mentry.getKey().equals("1/2"))
                flag = false;
        }
        System.out.println(" done\n");
        System.out.println("Result is: "+result);
    }
}
