//Output: Found result: 296962999629
//Runtime: real	0m1.320s

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class Prob49 {

    private final static int getDigitSum(int n) {
        int sum = 0;

        while(n > 0) {
            sum += n % 10;
            n /= 10;
        }

        return sum;
    }

    private final static boolean isPrime(int n) {
        if(n == 1)
            return false;

        else if(n == 2)
            return true;

        else {

            int i, r = (int) Math.floor(Math.sqrt(n));

            for(i = 2; i <= r && n % i != 0; ++i);

            return (r == i - 1);
        }
    }


    private final static boolean isPermutation(int a, int b) {
        String str1 = Integer.toString(a);
        String str2 = Integer.toString(b);

        if (str1.length() != str2.length())
            return false;
        else if (a == b)
            return false;

        char[] c = str1.toCharArray();
        char[] d = str2.toCharArray();

        Arrays.sort(c);
        Arrays.sort(d);

        return Arrays.equals(c, d);
    }

    private final static HashMap<Integer, Integer> generatePrimeRange(int min, int max) {
        HashMap<Integer, Integer> primeList = new HashMap<>();

        for (int i = min; i <= max; i++) {
            if (isPrime(i)) {
                primeList.put(i, getDigitSum(i));
            }
        }

        return primeList;
    }

    private final static void findPermutations(HashMap<Integer, Integer> h) {
        HashSet<Integer> primeList = new HashSet<>();

        for (int a : h.keySet()) {
            for (int b : h.keySet()) {
                if (h.get(a).equals(h.get(b))) {
                    if (isPermutation(a, b)) {
                        for(int c : h.keySet()) {
                            if( c != b && isPermutation(a, c) && (b-a == c-b) && a < b && b < c) {
                                primeList.add(a);
                                primeList.add(b);
                                primeList.add(c);
                                System.out.printf("Found result: %d%d%d%n",a, b, c);
                            }
                        }
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        HashMap<Integer, Integer> h = generatePrimeRange(1000, 9999);
        System.out.println("Generation finished.");
        findPermutations(h);
    }
}
