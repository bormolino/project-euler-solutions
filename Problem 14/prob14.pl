#!/usr/bin/perl -w

# Output: maxstep(837799)=525
# Runtime: real	0m18.938s

$m = 0;

foreach $i (2..1000000) {

    for($s = 1, $n = $i; $i > 1; $s++) {
        $i = ($i & 1) ? 3 * $i + 1 : $i / 2;
    }

    if($s > $m) {
        $m = $s;
        $t = "maxstep($n)=$s\n";
    }
}

print "$t\n";
