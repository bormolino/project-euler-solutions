#include <stdio.h>

/*
 * gcc -O3 -march=native -mtune=native -std=c11 -ffast-math -fopenmp Prob55.c -lm -o Prob55
 * Output: Result: 249!
 * Runtime: real	0m0.023s
 */

__uint128_t reverseInt(__uint128_t n) {
   __uint128_t r = 0;

   while (n != 0) {
      r *= 10;
      r += n % 10;
      n /= 10;
   }
   
   return r;
}

int palindromic(__uint128_t n) {
    return (n == reverseInt(n)) ? 0 : 1;
}

int main(void) {
    unsigned int res = 0, i, t;
    __uint128_t r;
	
    for(i = 0; i < 10000; i++) {
        r = i + reverseInt(i);

        for(t = 0; t < 50; t++) {
            if(palindromic(r) == 0)
                break;
            else
                r += reverseInt(r);
        }
		
        if(palindromic(r) == 1)
            res++;
    }
	
    printf("Result: %d!\n", res);
    return 0;
}
