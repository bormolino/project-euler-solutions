<?php

//Output: 648
//Runtime: real	0m0.046s

function getDigitSum($digits) {
    $strDigits = (string) $digits;

    for($intOut = $i = 0; $i < strlen($strDigits); $i++)
        $intOut += $strDigits{$i};

    return $intOut;
}

print getDigitSum(gmp_strval(gmp_fact(100)));

?>
