#!/usr/bin/perl -w

#Output: 871198282
#Runtime: real	0m0.036s

open FILE, "names.txt";
@file = <FILE>;
close FILE;
$i = 1;
$result = 0;

foreach $name (sort split ',', $file[0]) {
	$name =~ s/"//g;
	$value = 0;

	foreach $char (split '', $name) {
		$value += ord($char) - 64;
	}
	
	$result += $i * $value;
	$i++;
}

print "$result\n";

