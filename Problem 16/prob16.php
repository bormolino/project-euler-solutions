<?php

//Output: 1366
//Runtime: real	0m0.048s

$number = gmp_strval(gmp_pow("2",1000));

function getDigitSum($digits) {
	$strDigits = (string) $digits;

	for($intOut = $i = 0; $i < strlen($strDigits); $i++)
		$intOut += $strDigits{$i};

	return $intOut;
}

print(getDigitSum($number));

?>
